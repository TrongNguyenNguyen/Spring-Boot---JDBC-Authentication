package com.logicbig.example;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoderUtil {

	public static void main(String[] args) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String encoded = encoder.encode("123");
		System.out.println(encoded);
		encoded = encoder.encode("234");
		System.out.println(encoded);
	}
}
