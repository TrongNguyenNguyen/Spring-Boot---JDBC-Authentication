package com.logicbig.example;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class SpringBootJdbcAuthenticationApplication {

	@Bean
	public WebSecurityConfigurerAdapter webSecurityConfigurerAdapter(DataSource dataSource) {
		return new WebSecurityConfigurerAdapter() {

			@Override
			protected void configure(HttpSecurity http) throws Exception {
				http.authorizeRequests().anyRequest().authenticated().and().formLogin();
			}

			@Override
			protected void configure(AuthenticationManagerBuilder auth) throws Exception {
				auth.jdbcAuthentication().passwordEncoder(new BCryptPasswordEncoder()).dataSource(dataSource);
			}

		};
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJdbcAuthenticationApplication.class, args);
	}
}
