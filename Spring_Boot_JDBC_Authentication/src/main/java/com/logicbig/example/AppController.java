package com.logicbig.example;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AppController {

	@RequestMapping("/**")
	public String handler(ModelMap map, HttpServletRequest request) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		map.addAttribute("uri", request.getRequestURI());
		map.addAttribute("user", auth.getName());
		map.addAttribute("roles", auth.getAuthorities());
		return "app";
	}
}
